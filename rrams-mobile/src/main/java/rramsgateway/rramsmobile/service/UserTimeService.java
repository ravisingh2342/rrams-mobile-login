package rramsgateway.rramsmobile.service;

import org.springframework.stereotype.Service;
import rramsgateway.rramsmobile.dto.UserTime;
import rramsgateway.rramsmobile.repository.UserTimeRepository;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserTimeService {

    UserTimeRepository userTimeRepository;

    public  UserTimeService(UserTimeRepository userTimeRepository)
    {
        this.userTimeRepository=userTimeRepository;

    }

    public List<UserTime> getUsers()
    {
        List<UserTime> userTimeList=userTimeRepository.findAll();

        return  userTimeList;


    }

    public UserTime getUser(String userid)
    {
        //get user using lamda
        List<UserTime> userTimeList=userTimeRepository.findAll();

        List<UserTime> filteruserTimeList=  userTimeList.stream().filter(e -> e.getUserid().equals(userid)).collect(Collectors.toList());

        if(filteruserTimeList.size()>=1)
            return  filteruserTimeList.get(0);

        else
            return new UserTime("",Long.parseLong("0"),"","","","");

    }

    public String getSalt(String userid)
    {
        //get user using lamda
        List<UserTime> userTimeList=userTimeRepository.findAll();

        List<UserTime> filteruserTimeList=  userTimeList.stream().filter(e -> e.getUserid().equals(userid)).collect(Collectors.toList());

        if(filteruserTimeList.size()>=1)
            return  filteruserTimeList.get(0).getSalt();

        else
            return "NA";

    }



}
