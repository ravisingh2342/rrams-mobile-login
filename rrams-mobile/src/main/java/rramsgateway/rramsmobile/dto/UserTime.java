package rramsgateway.rramsmobile.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.bytebuddy.dynamic.loading.InjectionClassLoader;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String userid;
    private Long logintime;
    private String usercode;
    private String salt;
    private String password;
    private String macid;

}
