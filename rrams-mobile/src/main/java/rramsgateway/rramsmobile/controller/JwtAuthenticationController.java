package rramsgateway.rramsmobile.controller;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rramsgateway.rramsmobile.jwt.config.JwtTokenUtil;
import rramsgateway.rramsmobile.jwt.model.JwtRequest;
import rramsgateway.rramsmobile.jwt.model.JwtResponse;
import rramsgateway.rramsmobile.jwt.service.JwtUserDetailsService;
import rramsgateway.rramsmobile.service.UserTimeService;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private UserTimeService userTimeService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {


        System.out.println("password typed by user is..........................."+authenticationRequest.getPassword());

        //get salt from database......
        String salt=userTimeService.getSalt(authenticationRequest.getUsername());

        //do encryption with password and send for authentication........
       String newpassword= org.apache.commons.codec.digest.DigestUtils.md5Hex( authenticationRequest.getPassword()+salt);

        authenticate(authenticationRequest.getUsername(), newpassword);



        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}