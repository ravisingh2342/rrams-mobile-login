package rramsgateway.rramsmobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rramsgateway.rramsmobile.dto.UserTime;

public interface UserTimeRepository extends JpaRepository<UserTime,Long> {
}
