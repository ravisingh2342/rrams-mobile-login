package cicd.cicddemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @GetMapping("/")
    public String getMessage()
    {

        return "CICID CODE FROM PIPELINE AFTER TESTING";

    }


}
