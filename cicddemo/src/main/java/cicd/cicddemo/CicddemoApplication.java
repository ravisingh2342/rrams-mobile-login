package cicd.cicddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CicddemoApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CicddemoApplication.class, args);
	}

}
