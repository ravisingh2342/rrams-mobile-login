package in.rrams.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.rrams.gateway.dto.UserTime;



public interface UserTimeRepository extends JpaRepository<UserTime,Long> {
}
