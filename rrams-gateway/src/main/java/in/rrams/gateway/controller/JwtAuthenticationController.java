package in.rrams.gateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import in.rrams.gateway.dto.UserTime;
import in.rrams.gateway.dto.UserToken;
import in.rrams.gateway.jwt.config.JwtTokenUtil;
import in.rrams.gateway.jwt.model.JwtRequest;
import in.rrams.gateway.jwt.service.JwtUserDetailsService;
import in.rrams.gateway.service.UserTimeService;



@RestController
@CrossOrigin
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private UserTimeService userTimeService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {


       // System.out.println("password typed by user is..........................."+authenticationRequest.getPassword());

        //get salt from database......
        UserTime userTime=userTimeService.getSalt(authenticationRequest.getUsername());
        String salt=userTime.getSalt();
        //do encryption with password and send for authentication........
       String newpassword= org.apache.commons.codec.digest.DigestUtils.md5Hex( authenticationRequest.getPassword()+salt);

        authenticate(authenticationRequest.getUsername(), newpassword);



        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        UserToken resultToken = jwtTokenUtil.generateToken(userDetails);
        resultToken.setUsername(userTime.getUserid());
        resultToken.setUserid(userTime.getUsercode());
        return ResponseEntity.ok((resultToken));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}