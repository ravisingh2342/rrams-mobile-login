package in.rrams.gateway.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import in.rrams.gateway.dto.UserTime;
import in.rrams.gateway.service.UserTimeService;



@RestController
public class MyController {

        UserTimeService userTimeService;

        public MyController(UserTimeService userTimeService)
        {
            this.userTimeService=userTimeService;
        }

        @GetMapping("/hello")
        public String getMessage()
        {
            return "rrams-mobile";

        }

        @PostMapping("/getusers")
        public List<String> getUser()
        {

            return userTimeService.getUsers().stream().map(e -> e.getUserid()).collect(Collectors.toList());
        }

        @PostMapping("/getuser/{userid}")
        public UserTime getUser(@PathVariable("userid") String userid)
        {
            UserTime userTime=userTimeService.getUser(userid);
            return userTime;
        }


    }
