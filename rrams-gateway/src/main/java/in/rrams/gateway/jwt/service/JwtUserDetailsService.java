package in.rrams.gateway.jwt.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import in.rrams.gateway.dto.UserTime;
import in.rrams.gateway.service.UserTimeService;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    UserTimeService userTimeService;

    public JwtUserDetailsService(UserTimeService userTimeService)
    {
        this.userTimeService=userTimeService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

      UserTime userTime= userTimeService.getUser(username);


        if (userTime.getUserid().equals(username)) {
            return new User(userTime.getUserid(), userTime.getPassword(),
                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }
}
